function mode() {

    let bdy = document.querySelector('body')
    if (bdy.style.backgroundColor != "midnightblue") {
        bdy.style.backgroundColor = "midnightblue"
        bdy.style.color = "white"
    } else {
        bdy.style.backgroundColor = "white"
        bdy.style.color = "black"
    }
}
mode()